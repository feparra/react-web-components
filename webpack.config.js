const webpack = require("webpack")
const path = require("path")

const clientConfig = {
    mode: 'production',
    target: 'web',
    entry: {
        banner: "./src/shared/Banner/index.js",
        footer: "./src/shared/Footer/index.js"
    },
    output: {
        path: __dirname,
        filename: "./public/js/[name].js"
    },
    resolve: {
        modules: [
            path.resolve(__dirname, "src"),
            path.resolve(__dirname, "node_modules")
        ]
    },
    module: {
        rules: [
            {
                test: [/\.gif$/, /\.jpe?g$/, /\.png$/],
                loader: "file-loader",
                options: {
                    name: "public/media/[name].[ext]",
                    publicPath: url => url.replace(/public/, "")
                }
            },
            {   
                test: /\.svg$/,
                oneOf: [
                    { resourceQuery: /in/, use: 'svg-inline-loader'},
                    { resourceQuery: /ex/, use: 'file-loader' }
                ]
            },
            {
                test: /\.less$/,
                exclude: /(node_modules)/,
                use: [
                    "css-loader",
                    "less-loader",
                ]
            },
            {
                test: /\.(woff|woff2|ttf)$/, 
                loader: 'file-loader', 
                options: { name: 'public/fonts/[name].[ext]' }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    {loader: "babel-loader"}
                ]
            }
        ]
    }
}

const serverConfig = {
    mode: "production",
    target: "node",
    entry: "./src/server/index.js",
    output: {
        path: __dirname,
        filename: "./public/server.js",
        libraryTarget: "commonjs2"
    },
    module: {
        rules: [
            {
                test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                loader: "file-loader",
                options: {
                    name: "public/media/[name].[ext]",
                    publicPath: url => url.replace(/public/, ""),
                    emit: false
                }
            },
            {
                test: /\.less$/,
                use: [
                    "css-loader",
                    "less-loader"
                ]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    {loader: "babel-loader"}
                ]
            }
        ]
    }
}

module.exports = [clientConfig, serverConfig]
