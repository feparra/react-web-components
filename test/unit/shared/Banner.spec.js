import React from 'react'
import sinon from 'sinon'
import {expect} from 'helpers/chaiSetup'
import {shallow} from 'helpers/enzymeSetup'
import Contentful from 'clients/contentful'
import webComponentWrapper from 'libs/webComponentWrapper'

describe('Banner component', () => {
    let BannerComponent, mockPromise
    const sandbox = sinon.createSandbox()
    beforeEach(() => {
        const banner = {
            id: '1',
            alignment: 'left',
            title: 'test-title',
            paragraph: 'test-paragraph',
            link: {
                url: 'test-link-url',
                target: 'test-link-target',
                text: 'test-link-text'
            },
            image: {
                title: {
                    url: 'test-image-title-url',
                    alt: 'test-image-title-alt'
                },
                paragraph: 'test-image-paragraph',
                background: 'test-image-background'
            }
        }
        sandbox.stub(webComponentWrapper, 'registerComponent')
        const Banner = require('shared/Banner').default
        const bannerData = [banner, {...banner, id: '2', alignment: 'right'}, {...banner, id: '3', alignment: 'up'}]
        mockPromise = Promise.resolve(bannerData)
        sandbox.stub(Contentful, 'fetchBanner').returns(() => mockPromise)
        BannerComponent = shallow(<Banner source='test-source' business='bank' country='cl'/>)
    })

    afterEach(() => {
        sandbox.restore()
    })

    it('Have props', () => expect(BannerComponent.instance().props).to.have.property('source')
        .to.equal('test-source'))

    it('Have three items', () => mockPromise.then(() => expect(BannerComponent.find('article'))
        .to.have.lengthOf(3)))

    describe('First banner', () => {
        it('have key', () => mockPromise.then(() => expect(BannerComponent.find('article').first().key())
            .to.equal('1')))

        it('have classname', () => mockPromise.then(() => expect(BannerComponent.find('article').first())
            .to.have.className('single-banner align-left')))

        it('have style', () => mockPromise.then(() => expect(BannerComponent.find('article').first())
            .to.have.style('background-image')
            .to.equal('url(\'test-image-background?fm=jpg&q=50\')')))

        it('have title', () => mockPromise.then(() => expect(BannerComponent.find('article').first().find('h3'))
            .to.have.text('test-title')))

        it('have titleImage', () => mockPromise.then(() => { 
        const Img = BannerComponent.find('article').first().find('img').first()
        return expect(Img)
            .to.have.prop('src')
            .to.equal('test-image-title-url?q=50') &&
            expect(Img)
            .to.have.prop('alt')
            .to.equal('test-image-title-alt')
            }))
        
        it('have paragraph', () => mockPromise.then(() => expect(BannerComponent.find('article').first().find('p'))
            .to.have.prop('dangerouslySetInnerHTML')
            .to.deep.equal({__html: '<p>test-paragraph</p>\n'})))

        it('have paragraphImage', () => mockPromise.then(() => expect(BannerComponent.find('article').first().find('img').last())
            .to.have.prop('src')
            .to.equal('test-image-paragraph?fm=jpg&q=50')))

        it('have link', () => mockPromise.then(() => {
            const link = BannerComponent.find('article').first().find('a')
            return  expect(link)
                    .to.have.className('btn btn-secondary') &&
                expect(link)
                    .to.have.prop('href')
                    .to.equal('test-link-url') &&
                expect(link)
                    .to.have.prop('target')
                    .to.equal('test-link-target') &&
                expect(link)
                    .to.have.text('test-link-text')
            }))
    })

    describe('Second Banner', () => {
        it('have key', () => mockPromise.then(() => expect(BannerComponent.find('article').at(1).key())
            .to.equal('2')))

        it('have classname', () => mockPromise.then(() => expect(BannerComponent.find('article').at(1))
            .to.have.className('single-banner align-right')))
    })

    describe('Third Banner', () => {
        it('have key', () => mockPromise.then(() => expect(BannerComponent.find('article').last().key())
            .to.equal('3')))

        it('have classname', () => mockPromise.then(() => 
            expect(BannerComponent.find('article').last())
                .to.have.className('single-banner') && 
            expect(BannerComponent.find('article').last())
                .to.not.have.className('align-left') &&
            expect(BannerComponent.find('article').last())
                .to.not.have.className('align-right')))
    })
})
