import React from 'react'
import sinon from 'sinon'
import {expect} from 'helpers/chaiSetup'
import {shallow} from 'helpers/enzymeSetup'
import Contentful from 'clients/contentful'
import I18n from 'libs/i18n'
import ConfigByCountry from 'shared/Footer/configByCountry'

describe('Footer Component', () => {
    let FooterComponent, mockPromise, hasRestrictionStub, getSectionsStub, getTitleStub
    const intlProps = {language: 'es', business: 'cmr', country: 'mx'}
    const titles = {sectionOneTest: 'test-one-title', sectionTwoTest: 'test-two-title'}
    const footerData = { sectionOneTest: {}, sectionTwoTest: {} }
    const sandbox = sinon.createSandbox()

    beforeEach(() => {
        mockPromise = Promise.resolve(footerData)
        sandbox.stub(Contentful, 'fetchFooterLinks').resolves(mockPromise)
        getTitleStub = sandbox.stub(ConfigByCountry, 'getTitle').returns(titles)
        getSectionsStub = sandbox.stub(ConfigByCountry, 'getSections').returns({})
        hasRestrictionStub = sandbox.stub(ConfigByCountry, 'hasRestriction').returns(() => true)
        sandbox.stub(I18n, 'withRegister').returns(() => null)
        const Footer = require('shared/Footer').default
        FooterComponent = shallow(<Footer {...intlProps} />).find('Footer').shallow()
    })

    afterEach(() => {
        sandbox.restore()
    })

    it('renders footer section', () => {
        expect(FooterComponent.find('footer')).to.have.className('footer')
    })

    it('renders styles', () => {
        expect(FooterComponent.find('style')).to.be.present()
    })

    it('not rendes legal div', () => {
        expect(FooterComponent.find('.legality-container')).to.not.be.present()
    })

    it('render SocialNetwork component', () => {
        expect(FooterComponent.find('SocialNetworks')).to.be.present()
    })

    describe('fetching footer data', () => {
        it('has two General information component', () => mockPromise.then(() => {
            expect(FooterComponent.find('GeneralInformation')).to.have.lengthOf(2)
        }))
        it('first has props', () => mockPromise.then(() => {
            expect(FooterComponent.find('GeneralInformation').at(0)).to.have.prop('links').deep.equal({}) &&
            expect(FooterComponent.find('GeneralInformation').at(0)).to.have.prop('title').equal('test-one-title')
        }))
        it('second has props', () => mockPromise.then(() => {
            expect(FooterComponent.find('GeneralInformation').at(1)).to.have.prop('links').deep.equal({}) &&
            expect(FooterComponent.find('GeneralInformation').at(1)).to.have.prop('title').equal('test-two-title')
        }))
    })

    describe('When legal section is active', () => {
        beforeEach(() => {
            hasRestrictionStub.returns(() => false)
            const Footer = require('shared/Footer').default
            FooterComponent = shallow(<Footer {...intlProps} />).find('Footer').shallow()
        })
        it('has legality section', () => {
            expect(FooterComponent.find('.legality-container').find('Legality')).to.be.present()
        })
    })

    describe('When have additional information', () => {
        beforeEach(() => {
            getTitleStub.returns({...titles, sectionThreeTest: 'test-three-title', sectionFourTest: 'test-four-title'})
            getSectionsStub.returns({ sectionThreeTest: {}, sectionFourTest: {} })
            const Footer = require('shared/Footer').default
            FooterComponent = shallow(<Footer {...intlProps} />).find('Footer').shallow()
        })
        describe('fetching footer data', () => {
            it('has four General information component', () => mockPromise.then(() => {
                expect(FooterComponent.find('GeneralInformation')).to.have.lengthOf(4)
            }))
            it('first has props', () => mockPromise.then(() => {
                expect(FooterComponent.find('GeneralInformation').at(0)).to.have.prop('links').deep.equal({}) &&
                expect(FooterComponent.find('GeneralInformation').at(0)).to.have.prop('title').equal('test-one-title')
            }))
            it('second has props', () => mockPromise.then(() => {
                expect(FooterComponent.find('GeneralInformation').at(1)).to.have.prop('links').deep.equal({}) &&
                expect(FooterComponent.find('GeneralInformation').at(1)).to.have.prop('title').equal('test-two-title')
            }))
            it('third has props', () => mockPromise.then(() => {
                expect(FooterComponent.find('GeneralInformation').at(2)).to.have.prop('links').deep.equal({}) &&
                expect(FooterComponent.find('GeneralInformation').at(2)).to.have.prop('title').equal('test-three-title')
            }))
            it('four has props', () => mockPromise.then(() => {
                expect(FooterComponent.find('GeneralInformation').at(3)).to.have.prop('links').deep.equal({}) &&
                expect(FooterComponent.find('GeneralInformation').at(3)).to.have.prop('title').equal('test-four-title')
            }))
        })
    })    
})
