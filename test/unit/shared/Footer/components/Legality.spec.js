import React from 'react'
import {expect} from 'helpers/chaiSetup'
import {shallow} from 'helpers/enzymeSetup'
import Legality from 'shared/Footer/components/Legality'
import I18n from 'libs/i18n'
const {withI18n} = I18n

describe('Legality Component', () => {
    const MESSAGES = {test: {es: {xx: {
        SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_1: 'test-text-1',
        SITIO_PUBLICO_FOOTER_LEGALITY_GOVERNMENT_SITE: 'test-gob-site',
        SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_2: 'test-text-2',
        SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_3: 'test-text-3'
    }}}}
    const intlProps = {language: 'es', business: 'test', country: 'xx'}
    let shallowComponent

    beforeEach(() => {
        const ComponentWithDictionary = withI18n(MESSAGES)(Legality)
        shallowComponent = shallow(<ComponentWithDictionary {...intlProps} />).find(Legality).shallow()
    })

    it('renders first link id word', () => {
        expect(shallowComponent.find('FormattedMessage').first()).to.have.prop('id').equal('SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_1')
    })

    it('renders first link value href', () => {
        const link = shallowComponent.find('FormattedMessage').first().prop('values').LINK
        expect(link.props).to.have.property('href').equal('http://www.cmfchile.cl')
    })

    it('renders first link value target', () => {
        const link = shallowComponent.find('FormattedMessage').first().prop('values').LINK
        expect(link.props).to.have.property('target').equal('_blank')
    })

    it('renders first link value rel', () => {
        const link = shallowComponent.find('FormattedMessage').first().prop('values').LINK
        expect(link.props).to.have.property('rel').equal('external noopener noreferrer')
    })

    it('renders first link value text', () => {
        const link = shallowComponent.find('FormattedMessage').first().prop('values').LINK
        expect(link.props.children.props).to.have.property('id').equal('SITIO_PUBLICO_FOOTER_LEGALITY_GOVERNMENT_SITE')
    })

    it('renders second link id word', () => {
        expect(shallowComponent.find('FormattedMessage').at(1)).to.have.prop('id').equal('SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_2')
    })

    it('renders second link value href', () => {
        const link = shallowComponent.find('FormattedMessage').at(1).prop('values').LINK
        expect(link.props).to.have.property('href').equal('http://www.cmfchile.cl')
    })

    it('renders second link value target', () => {
        const link = shallowComponent.find('FormattedMessage').at(1).prop('values').LINK
        expect(link.props).to.have.property('target').equal('_blank')
    })

    it('renders second link value rel', () => {
        const link = shallowComponent.find('FormattedMessage').at(1).prop('values').LINK
        expect(link.props).to.have.property('rel').equal('external noopener noreferrer')
    })

    it('renders second link value text', () => {
        const link = shallowComponent.find('FormattedMessage').at(1).prop('values').LINK
        expect(link.props.children.props).to.have.property('id').equal('SITIO_PUBLICO_FOOTER_LEGALITY_GOVERNMENT_SITE')
    })

    it('renders last id word', () => {
        expect(shallowComponent.find('FormattedMessage').last()).to.have.prop('id').equal('SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_3')
    })
})
