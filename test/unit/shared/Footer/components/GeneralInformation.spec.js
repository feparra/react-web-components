import React from 'react'
import {expect} from 'helpers/chaiSetup'
import {shallow} from 'helpers/enzymeSetup'
import GeneralInformation from 'shared/Footer/components/GeneralInformation'
import I18n from 'libs/i18n'
const {withI18n} = I18n

describe('GeneralInformation Component', () => {
    const MESSAGES = {test: {es: {xx: {'TEST_TITTLE_WORD': 'test-title'}}}}
    const intlProps = {language: 'es', business: 'test', country: 'xx'}
    let shallowComponent

    beforeEach(() => {
        const ComponentWithDictinary = withI18n(MESSAGES)(GeneralInformation)
        const props = {
            title: 'TEST_TITTLE_WORD',
            links: [
                {
                    title: 'test-link-title',
                    url: 'test-link-url',
                    target: 'test-link-target'
                }
            ],
            ...intlProps
        }
        const shallowComponentWithDictionary = shallow(<ComponentWithDictinary {...props}/>)
        shallowComponent = shallowComponentWithDictionary.find(GeneralInformation).shallow()
    })

    it('renders title section', () => {
        expect(shallowComponent.find('FormattedMessage')).to.have.prop('id').equal('TEST_TITTLE_WORD')
    })

    it('renders one link item', () => {
        expect(shallowComponent.find('li')).to.have.lengthOf(1)
    })

    it('renders href link ', () => {
        expect(shallowComponent.find('a')).to.have.attr('href').equal('test-link-url')
    })

    it('renders target link ', () => {
        expect(shallowComponent.find('a')).to.have.attr('target').equal('test-link-target')
    })

    it('renders text link ', () => {
        expect(shallowComponent.find('a')).to.have.text('test-link-title')
    })
})
