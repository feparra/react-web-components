import React from 'react'
import {expect} from 'helpers/chaiSetup'
import {shallow} from 'helpers/enzymeSetup'
import SocialNetworks from 'shared/Footer/components/SocialNetworks'
import I18n from 'libs/i18n'
const {withI18n} = I18n

describe('SocialNetworks Component', () => {
    const MESSAGES = {test: {es: {xx: {SITIO_PUBLICO_FOOTER_HELP_BUTTON_TEXT: 'test-help'}}}}
    const intlProps = {language: 'es', business: 'test', country: 'xx'}
    let shallowComponent

    beforeEach(() => {
        const ComponentWithDictionary = withI18n(MESSAGES)(SocialNetworks)
        shallowComponent = shallow(<ComponentWithDictionary {...intlProps} />).find(SocialNetworks).shallow()
    })

    it('renders four icons', () => expect(shallowComponent.find('li')).to.have.lengthOf(4))

    describe('renders help link', () => {
        it('has className', () => expect(shallowComponent.find('a').at(0)).to.have.className('btn btn-white'))
        it('has href', () => expect(shallowComponent.find('a.btn')).to.have.prop('href').equal('/ayuda-y-contacto'))
        it('has text', () => expect(shallowComponent.find('a > FormattedMessage')).to.have.prop('id').equal('SITIO_PUBLICO_FOOTER_HELP_BUTTON_TEXT'))
    })

    describe('renders facebook icon', () => {
        let itemList
        beforeEach(() => {
            itemList = shallowComponent.find('li').at(0)
        })
        it('has href', () => expect(itemList.find('a')).to.have.prop('href').equal('https://www.facebook.com/bancofalabella'))
        it('has target', () => expect(itemList.find('a')).to.have.prop('target').equal('_blank') )
        it('has rel', () => expect(itemList.find('a')).to.have.prop('rel').equal('external noopener noreferrer'))
        it('has title', () => expect(itemList.find('a')).to.have.prop('title').equal('Facebook'))
        it('has icon', () => expect(itemList.find('i')).to.exist)
    })

    describe('renders twitter icon', () => {
        let itemList
        beforeEach(() => {
            itemList = shallowComponent.find('li').at(1)
        })
        it('has href', () => expect(itemList.find('a')).to.have.prop('href').equal('https://twitter.com/Banco_Falabella'))
        it('has target', () => expect(itemList.find('a')).to.have.prop('target').equal('_blank') )
        it('has rel', () => expect(itemList.find('a')).to.have.prop('rel').equal('external noopener noreferrer'))
        it('has title', () => expect(itemList.find('a')).to.have.prop('title').equal('Twitter'))
        it('has icon', () => expect(itemList.find('i')).to.exist)
    })

    describe('renders twitter icon', () => {
        let itemList
        beforeEach(() => {
            itemList = shallowComponent.find('li').at(2)
        })
        it('has href', () => expect(itemList.find('a')).to.have.prop('href').equal('https://www.instagram.com/banco_falabella/'))
        it('has target', () => expect(itemList.find('a')).to.have.prop('target').equal('_blank') )
        it('has rel', () => expect(itemList.find('a')).to.have.prop('rel').equal('external noopener noreferrer'))
        it('has title', () => expect(itemList.find('a')).to.have.prop('title').equal('Instagram'))
        it('has icon', () => expect(itemList.find('i')).to.exist)
    })

    describe('renders youtube icon', () => {
        let itemList
        beforeEach(() => {
            itemList = shallowComponent.find('li').at(3)
        })
        it('has href', () => expect(itemList.find('a')).to.have.prop('href').equal('https://www.youtube.com/user/BancoFalabellaChile'))
        it('has target', () => expect(itemList.find('a')).to.have.prop('target').equal('_blank') )
        it('has rel', () => expect(itemList.find('a')).to.have.prop('rel').equal('external noopener noreferrer'))
        it('has title', () => expect(itemList.find('a')).to.have.prop('title').equal('Youtube'))
        it('has icon', () => expect(itemList.find('i')).to.exist)
    })
})

