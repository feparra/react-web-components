#!/bin/bash

NPM_COMMAND=$1
DOCKER_COMMAND="docker run --rm -v $PWD/:/app react-web-component-test /bin/sh -c \"(mv node_modules original_node_modules 2> /dev/null || echo 'dont backup node_modules' && true) && ln -s /tmp/node_modules node_modules && $NPM_COMMAND && unlink node_modules && (mv original_node_modules node_modules 2> /dev/null || echo 'dont restore node_modules' && true)\""
eval $DOCKER_COMMAND
