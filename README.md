# React Web Components

Este proyecto es una prueba de concepto para crear web components reutilizables en react, la idea es poder crear componentes independientes para sitio público.

### MVPS's
1. Crear un web component Banner y disponibilizarlo mediante servidor.

### Tecnologías

Listado de las principales librerias usadas.

* [ES6]: Estandar javascript principalmente usado.
* [React]: Creación de componentes React
* [React-Web-Component]: Creación de Web components bajo el estándar aceptado por los browsers.
* [Axios]: Llamado a servicios
* [Express]: Servidor de contenido
* [Podium]: Routing de recursos server side rending
* [Webpack]: Generador de bundles y configuración del proyecto
* [Classnames]: Asignación de clases css más dinámicas a los tags html

### Instalación

Debes instalar la herramienta NVM, con esto puedes manejar las versiones de node y npm de este proyecto independiente de cualquier otro que tengas, puedes usar el comando siguiente, o revisar la [página de nvm](https://github.com/nvm-sh/nvm).
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
```
Luego ejecutar:
```
nvm use
npm i
```
Con esto ya habrás instalado todo lo necesario.

### Desarrollo

Existen varios comandos útiles, los cuales se listan a continuación:

* **npm run build**: Construye las componentes según la configuracion de webpack.
* **npm run up**: Ejecuta el servidor que debe estar ya construido.
* **npm start**: Ejecuta build y up en conjunto.
* **npm run lint**: Ejecuta el análisis sintactico usando eslint.
* **npm run test:unit**: Ejecuta las pruebas unitarias.
* **npm run test:unit:watch**: Ejecuta las pruebas en modo TDD.
* **npm run ci**: Ejecuta todas las tareas de integracion continua.
* **npm run docker:build**: Construye una imagen docker con las dependencias(node_modules) del proyecto.
* **npm run docker:run:lint**: Ejecuta la tarea lint dentro del contendor.
* **npm run docker:run:test:unit**: Ejecuta la tarea test:unit dentro del contenedor.
* **npm run docker:run:ci**: Ejecuta la tarea ci dentro del contendor.
* **npm run docker:lint**: Reconstruye la imagen docker y ejecuta la tarea lint dentro del contendor.
* **npm run docker:test:unit**: Reconstruye la imagen docker y ejecuta la tarea test:unit dentro del contendor.
* **npm run docker:ci**: Reconstruye la imagen docker y ejecuta la tarea ci dentro del contendor.
