import React from 'react'
import {FormattedMessage} from 'react-intl'

const BUSINESS_CONFIG_MAP = {
    'bank-cl': {
        titles: {
            'nuestro-banco': 'SITIO_PUBLICO_FOOTER_INFORMATION_BANK_TITLE',
            'servicio-al-cliente': 'SITIO_PUBLICO_FOOTER_INFORMATION_HELP_DESK_TITLE',
        }
    },
    'cmr-mx': {
        titles: {
            'sobre-nosotros': 'SITIO_PUBLICO_FOOTER_INFORMATION_ABOUT_US_TITLE',
            'terminos-legales': 'SITIO_PUBLICO_FOOTER_INFORMATION_LEGAL_TERMS_TITLE',
            'descarga-app': 'SITIO_PUBLICO_FOOTER_INFORMATION_APP_MOBILE_TITLE',
            'contacta-con-nosotros': 'SITIO_PUBLICO_FOOTER_INFORMATION_CONTACT_US_TITLE'
        },
        restrictions: [
            'legality'
        ],
        sections: {
            'descarga-app': [ 
                {
                    title: (<img src='https://images.ctfassets.net/s6vonmy5x76n/2ByoFKaGikggE68mywM8oI/a6475b43ef9967b5c19fdf1cf2d45f18/boton-app-store.png?q=60' alt='IOS app store'></img>),
                    url: 'https://itunes.apple.com/mx/app/tarjeta-falabella-m%C3%A9xico/id1360391012?l=en&amp;mt=8',
                    target: '_blank'
                },
                {
                    title: (<img src='https://images.ctfassets.net/s6vonmy5x76n/6myWBTHudaUsKOGGgWYGew/55e7dd4b84eb8ca28464f06b1df325b7/boton-google-play.png?q=60' alt='Google play store'></img>),
                    url: 'https://play.google.com/store/apps/details?id=mx.falabella.production',
                    target: '_blank'
                }
            ],
            'contacta-con-nosotros': [
                {
                    title: (<span>
                            <FormattedMessage id='SITIO_PUBLICO_FOOTER_INFORMATION_CONTACT_US_TEXT_LINK_1' description='text for link'/>
                            <br/>
                            <b><FormattedMessage id='SITIO_PUBLICO_FOOTER_INFORMATION_CONTACT_US_PHONE_LINK_1' description='text for phone'/></b>
                            </span>),
                    url: 'tel:018007674262',
                    target: '_self'
                },
                {
                    title: (<span>
                            <FormattedMessage id='SITIO_PUBLICO_FOOTER_INFORMATION_CONTACT_US_TEXT_LINK_2' description='text for link'/>
                            <br/>
                            <b><FormattedMessage id='SITIO_PUBLICO_FOOTER_INFORMATION_CONTACT_US_PHONE_LINK_2' description='text for phone'/></b>
                            </span>),
                    url: 'tel:+5550894262',
                    target: '_self'
                },
                {
                    title: <FormattedMessage id='SITIO_PUBLICO_FOOTER_INFORMATION_CONTACT_US_SUBSIDIARY' description='text for link'/>,
                    url: '/sucursales',
                    target: '_self'
                },
                {
                    title: <FormattedMessage id='SITIO_PUBLICO_FOOTER_INFORMATION_CONTACT_US_FAQ' description='text for link'/>,
                    url: '/faqs',
                    target: '_self'
                }
            ]
        }
    }
}


const buildKeyMap = (business, country) => [business, country].join('-')
const getTitle = ({business, country}) => BUSINESS_CONFIG_MAP[buildKeyMap(business, country)].titles
const getSections = ({business, country}) => BUSINESS_CONFIG_MAP[buildKeyMap(business, country)].sections || []
const hasRestriction = ({business, country}) => (restriction) => {
    const keyMap = buildKeyMap(business, country)
    if(BUSINESS_CONFIG_MAP[keyMap].restrictions !== undefined) {
       return BUSINESS_CONFIG_MAP[keyMap].restrictions.includes(restriction)
    }
    return false
}

export default {getTitle, getSections, hasRestriction}
