import React from 'react'
import {FormattedMessage} from 'react-intl'

const buildLinks = (links) => links.map(({title, url, target}, index) => (<li key={index}><a href={url} target={target}>{title}</a></li>))

const GeneralInformation = (props) => (
    <section className='general-information'>
        <h4><FormattedMessage id={props.title} description="Title for general information"/></h4>
        <ul>
            {buildLinks(props.links)}
        </ul>
    </section>
)

export default GeneralInformation
