import React from 'react'
import {FormattedMessage} from 'react-intl' 

const buildLink = (idWord) => (<a href='http://www.cmfchile.cl' target='_blank' rel='external noopener noreferrer'><FormattedMessage id={idWord} description={`link for id ${idWord}`}/></a>)

const Legality = () => (
    <section className='legality'>
        <p>
            <FormattedMessage 
                id='SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_1' 
                description='text about government regulations'
                values={{LINK: buildLink('SITIO_PUBLICO_FOOTER_LEGALITY_GOVERNMENT_SITE')}}/>
        </p>
        <p>
            <FormattedMessage 
                id='SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_2' 
                description='text about link to government site'
                values={{LINK: buildLink('SITIO_PUBLICO_FOOTER_LEGALITY_GOVERNMENT_SITE')}}/>
            <FormattedMessage 
                id='SITIO_PUBLICO_FOOTER_LEGALITY_TEXT_3'
                description='text about copy right'/>
        </p>
    </section>
)

export default Legality
