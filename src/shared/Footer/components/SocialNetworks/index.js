import React from 'react'
import {FormattedMessage} from 'react-intl'
import PhoneSVG from './phone.svg?in'
import FacebookSVG from './facebook.svg?in'
import TwitterSVG from './twitter.svg?in'
import InstagramSVG from './instagram.svg?in'
import YoutubeSVG from './youtube.svg?in'

const SocialNetworks = () => (
    <section className='social-networks'>
        <a className='btn btn-white' href='/ayuda-y-contacto'>
            <i dangerouslySetInnerHTML={{__html: PhoneSVG}}></i><FormattedMessage id='SITIO_PUBLICO_FOOTER_HELP_BUTTON_TEXT' description='Text for help button'/></a>
        <ul>
            <li><a href='https://www.facebook.com/bancofalabella' target='_blank' rel='external noopener noreferrer' title='Facebook'>
                <i dangerouslySetInnerHTML={{__html: FacebookSVG}}></i>
            </a></li>
            <li><a href='https://twitter.com/Banco_Falabella' target='_blank' rel='external noopener noreferrer' title='Twitter'>
                <i dangerouslySetInnerHTML={{__html: TwitterSVG}}></i>
            </a></li>
            <li><a href='https://www.instagram.com/banco_falabella/' target='_blank' rel='external noopener noreferrer' title='Instagram'>
                <i dangerouslySetInnerHTML={{__html: InstagramSVG}}></i>
            </a></li>
            <li><a href='https://www.youtube.com/user/BancoFalabellaChile' target='_blank' rel='external noopener noreferrer' title='Youtube'>
                <i dangerouslySetInnerHTML={{__html: YoutubeSVG}}></i>
            </a></li>
        </ul>
    </section>
)

export default SocialNetworks
