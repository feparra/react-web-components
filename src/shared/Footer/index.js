import React, {Component} from 'react'
import PropTypes from 'prop-types'
import I18n from 'libs/i18n'
import Contentful from 'clients/contentful'
import GeneralInformation from 'shared/Footer/components/GeneralInformation'
import SocialNetworks from 'shared/Footer/components/SocialNetworks'
import Legality from 'shared/Footer/components/Legality'
import ConfigByCountry from './configByCountry'

import styles from './styles.less'
import i18nJSON from './i18n.json'

class Footer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sections: {}
        }
    }

    componentDidMount() {
        Contentful.fetchFooterLinks(this.props).then(sections => this.setState({sections}))
    }

    buildGeneralInformation = (sections) => Object.keys(sections)
    .map(section => (<GeneralInformation key={section} links={sections[section]} title={ConfigByCountry.getTitle(this.props)[section]} />))

    buildAdditionalInformation = () => this.buildGeneralInformation(ConfigByCountry.getSections(this.props))

    buildLegality = () => (ConfigByCountry.hasRestriction(this.props)('legality')) ? null : (<div className='legality-container'><Legality /></div>)

    render() {
        return (
        <div>
            <style>
                {styles.toString()}
            </style>
            <footer className='footer'>
                <div className='information-container'>
                    {this.buildGeneralInformation(this.state.sections)}
                    {this.buildAdditionalInformation()}
                    <SocialNetworks />
                </div>
                {this.buildLegality()}
            </footer>
        </div>
        )
    }
}

Footer.propTypes = {
    business: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    env: PropTypes.string
}

const FooterWithInt = I18n.withI18n(i18nJSON)(Footer)
I18n.withRegister('footer-component', [])(FooterWithInt)
export default FooterWithInt
