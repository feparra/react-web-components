import React, {Component} from 'react'
import PropTypes from 'prop-types'
import webComponentWrapper from '../../libs/webComponentWrapper'
import classnames from 'classnames'
import marked from 'marked'
import Contentful from '../../clients/contentful'

import styles from './styles.less' 

class Banner extends Component {
    constructor(props) {
        super(props)
        this.state = {
            singleBanner: []
        }
    }

    componentDidMount() {
        Contentful.fetchBanner(this.props.business, this.props.country)(this.props.source).then(response => this.setState({singleBanner: response}))
    }

    buildSingleBanner = () => this.state.singleBanner.map(banner => {
        const style = { backgroundImage: `url('${banner.image.background}?fm=jpg&q=50')` }
        const title = (banner.title) ? (<h3>{banner.title}</h3>) : null
        const titleImage = (banner.image.title) ? (<img src={`${banner.image.title.url}?q=50`} alt={banner.image.title.alt} />) : null
        const paragraph = (banner.paragraph) ? (<p dangerouslySetInnerHTML={{__html: marked(banner.paragraph)}} />) : null
        const paragraphImage = (banner.image.paragraph) ? (<img src={`${banner.image.paragraph}?fm=jpg&q=50`} alt='for paragraph' />) : null
        const link = (banner.link) ? (<a className='btn btn-secondary' href={banner.link.url} target={banner.link.target}>{banner.link.text}</a>) : null
        const bannerClass = classnames('single-banner', {'align-left': banner.alignment === "left", 'align-right': banner.alignment === "right"})
        return (<article key={banner.id} className={bannerClass} style={style}>
            <div className='info-container'>
                <div className='information'>
                    {title}
                    {titleImage}
                    {paragraph}
                    {paragraphImage}
                    {link}
                </div>
            </div>
        </article>)
    })

    render() {
        
        return (
            <div>
                <style>
                    {styles}
                </style>
                <section className='banner'>
                    {this.buildSingleBanner()}
                </section>
            </div>
        )
    }
}
Banner.propTypes = {
    source: PropTypes.string.isRequired,
    business: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired
}

webComponentWrapper.registerComponent(Banner, 'banner-component', ['source', 'business', 'country'])
export default Banner
