import express from 'express'
import Podlet from '@podium/podlet'

const app = express()
const bannerPodlet = new Podlet({
    name: 'Banner',
    version: '1.0.0',
    pathname: '/',
    manifest: 'manifest.json',
    content: '/banner',
    development: true
})
const footerPodlet = new Podlet({
    name: 'Footer',
    version: '1.0.0',
    pathname: '/',
    manifest: 'manifest.json',
    content: '/footer',
    development: true
})

app.use(bannerPodlet.middleware())
app.use(footerPodlet.middleware())


app.use('/banner/js', express.static('./public/js'))
app.use('/footer/js', express.static('./public/js'))


app.get(`${bannerPodlet.content()}/:business/:country/:language`, (req, res) => {
    const {business, country} = req.params
    const {source} = req.query
    res.status(200).podiumSend(`
        <script src="/banner/js/banner.js"></script>
        <banner-component source="${source}" business="${business}" country="${country}"></banner-component>
`)
})

app.get(`${footerPodlet.content()}/:env/:business/:country/:language`, (req, res) => {
    const {env, business, country, language} = req.params
    res.status(200).podiumSend(`
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
        <style>
            body {
                margin: 0;
                padding: 0;
            }
        </style>
        <script src="/footer/js/footer.js"></script>
        <footer-component env="${env}" business="${business}" country="${country}" language="${language}"></footer-component>
`)
})

app.get(bannerPodlet.manifest(), (req, res) => {
    res.status(200).send(bannerPodlet)
})

app.get(footerPodlet.manifest(), (req, res) => {
    res.status(200).send(footerPodlet)
})

app.listen(process.env.PORT || 8080, () => console.log("Server Up"))
