const CONTENFUL_SPACES = {
    dev: {
        host: 'https://preview.contentful.com',
        'bank-cl': {
            space: 'p6eyia4djstu',
            credential: 'b1b552df83beedaa93f5a5f874d5b43d12d755fbf2fe3835bdc25d7b31195a18'
        },
        'cmr-mx': {
            space: 'awjurk6f9seg',
            credential: '1ae72e418f9dd0e0506d21a5f95d001de844f0dd4fc7016ab0f9c9f2f458d0af'
        }
    },
    prod: {
        host: 'https://cdn.contentful.com',
        'bank-cl': {
            space: 'p6eyia4djstu',
            credential: '560c0ddde9630e43122ef3e7879d69013844ee3d48c566d4ba93125924f080dd'
        },
        'cmr-mx': {
            space: 'awjurk6f9seg',
            credential: '480aa34b241409f0e57c96859c5c878c691b247aae5360e6df8a43fd1bf221d1'
        }
    }
}
const buildKey = (business, country) => [business, country].join('-')

export const isMobile = () => window.innerWidth <= 768
export const getContentfulHost = (env = 'dev') => CONTENFUL_SPACES[env].host
export const getContentfulSpace = (business, country, env = 'dev') => CONTENFUL_SPACES[env][buildKey(business, country)].space
export const getContentfulCredentials = (business, country, env = 'dev') => ['Bearer', CONTENFUL_SPACES[env][buildKey(business, country)].credential].join(' ')
