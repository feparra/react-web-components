let webReactComponents = null;

const registerComponent = (reactComponent, tagName, attributesName) => {
    if(webReactComponents === null) {
        webReactComponents = require('web-react-components')
    }
    const defaultAttributes = ['env']
    webReactComponents.register(reactComponent, tagName, [...attributesName, ...defaultAttributes])
}

export default {registerComponent}
