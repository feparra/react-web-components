import React, {Component} from 'react'
import {IntlProvider, addLocaleData} from 'react-intl'
import esLocaleData from 'react-intl/locale-data/es';
import WebComponent from 'libs/webComponentWrapper'

addLocaleData([...esLocaleData])

const withI18n = (dictionary) => (ReactComponent) => {
    return class extends Component {
        render() {
            const componentDictionary = dictionary[this.props.business][this.props.language][this.props.country]
            return (
                <IntlProvider key={this.props.language} locale={this.props.language} messages={componentDictionary}>
                    <ReactComponent {...this.props} />
                </IntlProvider>
            )
        }
    }
}

const withRegister = (tagName, attributesName) => (reactComponent) => {
    const i18nAttributes = ["business", "language", "country"]
    WebComponent.registerComponent(reactComponent, tagName, [...i18nAttributes, ...attributesName])
}

export default {withI18n, withRegister}
